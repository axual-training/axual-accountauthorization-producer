package io.axual.training.generator;


import io.axual.general.User;
import io.axual.payments.AccountId;
import io.axual.payments.IBAN;
import io.axual.training.common.Account;

import java.util.*;

/**
 * Stores and supplies User IDs for all accounts
 */
public class IdStore {
    private Random random = new Random();
    private final Map<AccountId, List<User>> idMap = new HashMap<>();
    private final static String ID_PREFIX = "axual-demo-id-";
    private final static String ID_PREFIX_USER_ONLY = "axual-demo-uid-";

    public IdStore(List<Account> accounts) {
        List<String> sortedList = new ArrayList<>();
        Map<String, AccountId> identificationFinder = new HashMap<>();
        for (Account account : accounts) {
            String iban = ((IBAN) account.getIdentification().getId()).getAccountNumber();
            identificationFinder.put(iban, account.getIdentification());
            sortedList.add(iban);
        }
        Collections.sort(sortedList);
        int counter = 0;
        for (String iban : sortedList) {
            idMap.put(identificationFinder.get(iban), generateIds(counter));
            counter++;
        }
    }

    private List<User> generateIds(int counter) {
        String customerId = ID_PREFIX + counter;
        List<User> ids = new ArrayList<>();
        if ((counter % 2) == 0) {
            ids.add(User.newBuilder().setCustomerId(customerId).setUserId(customerId).build());
        }
        int userCounter = counter;
        while (ids.size() < 4) {
            String userId = ID_PREFIX_USER_ONLY + userCounter;
            ids.add(User.newBuilder().setCustomerId(customerId).setUserId(userId).build());
            userCounter++;
        }
        return ids;
    }

    public List<User> getAllPossibleIds(AccountId identification) {
        return idMap.get(identification);
    }

    public List<User> getRandomIds(AccountId identification) {
        List<User> all = getAllPossibleIds(identification);
        Collections.shuffle(all, random);
        List<User> randomIds = new ArrayList<>();
        int size = random.nextInt(5);
        for (int i = 0; i < size; i++) {
            randomIds.add(all.get(i));
        }
        return randomIds;
    }
}
